const express = require("express");
const morgan = require("morgan");
const jwt = require("jsonwebtoken");
var cors = require("cors");

// Initializations
const app = express();

// Settings
app.set("port", process.env.PORT || 4000);

// Middlewares
app.use(morgan("dev"));
app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

// Global Variables
app.use((req, res, next) => {
  next();
});

// Routes
app.use(require("./routes"));
app.use("/auth", require("./routes/authentication"));
app.use("/prospect", validateToken, require("./routes/prospects"));
app.use("/historic", validateToken, require("./routes/historic"));

// Starting the server
app.listen(app.get("port"), () => {
  console.log(`App Listen on port ${app.get("port")}`);
});

// Session
function validateToken(req, res, next) {
  const accessToken = req.headers["authorization"];
  if (!accessToken) res.status(401).send({ message: "Acceso denegado" });

  jwt.verify(accessToken, process.env.SECRET_KEY, (err, user) => {
    if (err) {
      res
        .status(401)
        .send({ message: "Acceso denegado, token expirado o incorrecto" });
    } else {
      req.user = user;
      next();
    }
  });
}
