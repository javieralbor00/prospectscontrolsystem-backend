const express = require("express");
const router = express.Router();
const pool = require("../databse");

// Get prospect list
router.get("/", async (req, res) => {
  const prospectList = await pool.query("SELECT * FROM prospects");

  if (prospectList.length === 0) {
    res.status(204).send();
    return;
  }

  const mappedProspects = prospectList.map((prospect) => {
    const createdDate = new Date(prospect.created_at);
    const birthdayDate = new Date(prospect.birthday);

    const humanCreatedDate = `${createdDate.getDate()}/${
      createdDate.getMonth() + 1
    }/${createdDate.getFullYear()} ${createdDate.getHours()}:${createdDate.getMinutes()}:${createdDate.getSeconds()}`;

    const humanBirthdayDate = `${birthdayDate.getDate()}/${
      birthdayDate.getMonth() + 1
    }/${birthdayDate.getFullYear()}`;

    return {
      ...prospect,
      created_at: humanCreatedDate,
      birthday: humanBirthdayDate,
    };
  });
  res.send({ prospectList: mappedProspects });
});

// Create a prospect
router.post("/add", async (req, res) => {
  const { name, birthday, email } = req.body;

  if (!name || !birthday || !email) {
    res.status(400).send({
      message: "El nombre, fecha de nacimiento y correo son requeridos.",
    });
    return;
  }

  const prospect = await pool.query("SELECT * FROM prospects WHERE email = ?", [
    email,
  ]);

  if (prospect[0]) {
    res.status(409).send({
      message: "El prospecto con email " + email + " ya existe.",
    });
  } else {
    // Get user from token
    const { user } = req.user;

    const newProspect = {
      name,
      birthday,
      email,
      status: "Nuevo",
      created_by: user.username,
    };

    await pool.query("INSERT INTO prospects set ?", [newProspect]);

    res.send({ message: "Prospecto agregado correctamente.", newProspect });
  }
});

// Delete a prospect
router.delete("/delete/", async (req, res) => {
  const { email } = req.body;

  if (!email) {
    res.status(400).send({
      message: "El email es requerido.",
    });
    return;
  }

  await pool.query("DELETE FROM prospects WHERE email = ?", [email]);

  res.send({
    message:
      "El prospecto con email " + email + " se elimino satisfactoriamente.",
  });
});

// Get a single prospect
router.get("/singleProspect/:email", async (req, res) => {
  const { email } = req.params;

  const prospect = await pool.query("SELECT * FROM prospects WHERE email = ?", [
    email,
  ]);

  if (prospect.length > 0) {
    res.send({ prospect: prospect[0] });
  } else {
    res.status(204).send();
  }
});

// Update a prospect
router.post("/update/", async (req, res) => {
  const { email, status } = req.body;

  if (!email || !status) {
    res.status(400).send({
      message: "El email y el estatus son requerido.",
    });
    return;
  }

  const prospect = await pool.query("SELECT * FROM prospects WHERE email = ?", [
    email,
  ]);

  if (prospect[0]) {
    const updatedProspect = {
      ...prospect[0],
      status,
    };

    // Get user from token
    const { user } = req.user;

    await pool.query("UPDATE prospects set ? WHERE email = ?", [
      updatedProspect,
      email,
    ]);

    if (prospect[0].status !== status) {
      await pool.query("INSERT INTO historic set ?", [
        {
          updated_by: user.username,
          old_status: prospect[0].status,
          new_status: status,
          prospect_email: prospect[0].email,
        },
      ]);
    }

    res.send({
      message: "El prospecto con email " + email + " se actualizo con exito.",
      updatedProspect,
    });
  } else {
    res.status(403).send({
      message: "El prospecto con email " + email + " no existe.",
    });
  }
});

module.exports = router;
