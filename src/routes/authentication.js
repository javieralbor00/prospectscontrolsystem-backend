const express = require("express");
const router = express.Router();
const pool = require("../databse");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
require("dotenv").config();

router.post("/sign-up", async (req, res) => {
  const { username, name, password } = req.body;

  const user = await pool.query("SELECT * FROM users WHERE username = ?", [
    username,
  ]);

  if (!username || !name || !password) {
    res.status(400).send({
      message: "El nombre de usuario, nombre, y contraseña son requeridos.",
    });
    return;
  }

  if (user[0]) {
    res.status(409).send({
      message: "El nombre de usuario " + username + " ya existe.",
    });
  } else {
    const encryptedPassword = await encryptPassword(password);
    const newUser = {
      username,
      name,
      password: encryptedPassword,
    };
    await pool.query("INSERT INTO users set ?", [newUser]);

    res.status(200).send({
      message: "!Registro exitoso!",
    });
  }
});

router.post("/login", async (req, res) => {
  const { username, password } = req.body;

  if (!username || !password) {
    res.status(400).send({
      message: "El nombre de usuario y la contraseña son requeridos.",
    });
    return;
  }

  const user = await pool.query("SELECT * FROM users WHERE username = ?", [
    username,
  ]);

  if (user[0] && (await matchPassword(password, user[0].password))) {
    const accessToken = await generateAccessToken(user[0]);
    delete user[0]["password"];

    res.status(200).header("authorization", accessToken).json({
      message: "Usuario autenticado",
      token: accessToken,
      user: user[0],
    });
  } else {
    res.status(403).send({
      message: "El nombre de usuario o la contraseña son incorrectos.",
    });
  }
});

const encryptPassword = async (password) => {
  const salt = await bcrypt.genSalt(10);
  const hash = bcrypt.hash(password, salt);
  return hash;
};

const matchPassword = async (password, savedPassword) => {
  try {
    const isCorrectPassword = await bcrypt.compare(password, savedPassword);
    return isCorrectPassword;
  } catch (error) {
    console.log(error);
  }
};

function generateAccessToken(user) {
  return jwt.sign({ user }, process.env.SECRET_KEY, { expiresIn: "24h" });
}

module.exports = router;
