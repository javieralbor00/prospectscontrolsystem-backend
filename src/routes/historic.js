const express = require("express");
const router = express.Router();
const pool = require("../databse");

// Get historic
router.get("/", async (req, res) => {
  const historic = await pool.query("SELECT * FROM historic");

  if (historic.length === 0) {
    res.status(204).send();
    return;
  }

  res.send({ historic });
});

// Get historic by prospect
router.get("/:email", async (req, res) => {
  const { email } = req.params;
  const historic = await pool.query(
    "SELECT * FROM historic WHERE prospect_email = ?",
    [email]
  );

  if (historic.length === 0) {
    res.status(204).send();
    return;
  }

  res.send({ historic });
});

module.exports = router;
